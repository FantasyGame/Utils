﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FantasyGame.Utils
{
    public static class TimeHelper
    {
        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long Now()
        {
            return Convert.ToInt64((DateTime.Now - epoch).TotalMilliseconds);
        }

        public static long UtcNow()
        {
            return Convert.ToInt64((DateTime.UtcNow - epoch).TotalMilliseconds);
        }

        public static long NowSeconds()
        {
            return Convert.ToInt64((DateTime.Now - epoch).TotalSeconds);
        }

        public static long UtcNowSeconds()
        {
            return Convert.ToInt64((DateTime.UtcNow - epoch).TotalSeconds);
        }

        public static long NowTicks()
        {
            return Convert.ToInt64((DateTime.Now - epoch).Ticks);
        }

        public static long UtcNowTicks()
        {
            return Convert.ToInt64((DateTime.UtcNow - epoch).Ticks);
        }
    }
}
