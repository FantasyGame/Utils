﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace FantasyGame.Utils
{
    /// <summary>
    /// Json序列化工具类
    /// </summary>
    public static class JsonHelper
    {
        #region JObject和Json相互转换
        /// <summary>
        /// 将JObject转换为Json字符串
        /// </summary>
        /// <param name="jo"></param>
        /// <returns></returns>
        public static string JObject2Json(JObject jo, Formatting formatting = Formatting.None)
        {
            return jo.ToString(formatting);
        }

        /// <summary>
        /// 将Json转换为JObject
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static JObject Json2JObject(string json)
        {
            return JObject.Parse(json);
        }
        #endregion

        #region JArray和Json相互转换
        /// <summary>
        /// 将JArray转换为Json
        /// </summary>
        /// <param name="ja"></param>
        /// <returns></returns>
        public static string JArray2Json(JArray ja, Formatting formatting = Formatting.None)
        {
            return ja.ToString(formatting);
        }

        /// <summary>
        /// 将Json转换为JArray
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static JArray Json2JArray(string json)
        {
            return JArray.Parse(json);
        }
        #endregion

        #region Object和Json相互转换
        /// <summary>
        /// 将Object转换为Json
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string Object2Json(object obj, Formatting formatting = Formatting.None)
        {
            return JsonConvert.SerializeObject(obj, formatting);
        }

        /// <summary>
        /// 将Json转换为Object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T Json2Object<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// 将Json转换为Object并且覆盖到原有对象中
        /// </summary>
        /// <param name="json"></param>
        /// <param name="target"></param>
        public static void JsonCover2Object(string json, object target)
        {
            JsonConvert.PopulateObject(json, target);
        }
        #endregion

        #region Dictionary和Json相互转换
        /// <summary>
        /// 将Dictionary转换为Json
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dic"></param>
        /// <returns></returns>
        public static string Dictionary2Json<TKey, TValue>(Dictionary<TKey, TValue> dic, Formatting formatting = Formatting.None)
        {
            Dictionary<string, TValue> d = new Dictionary<string, TValue>();
            foreach (KeyValuePair<TKey, TValue> kv in dic)
            {
                d.Add(Object2Json(kv.Key), kv.Value);
            }

            return Object2Json(d, formatting);
        }

        /// <summary>
        /// 将Json转换为Dictionary
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static Dictionary<TKey, TValue> Json2Dictionary<TKey, TValue>(string json)
        {
            Dictionary<string, TValue> d1 = Json2Object<Dictionary<string, TValue>>(json);
            Dictionary<TKey, TValue> d2 = new Dictionary<TKey, TValue>();
            foreach (KeyValuePair<string, TValue> kv in d1)
            {
                d2.Add(Json2Object<TKey>(kv.Key), kv.Value);
            }

            return d2;
            //return Json2Object<Dictionary<TKey, TValue>>(json);
        }
        #endregion

        #region List和Json相互转换
        /// <summary>
        /// 将List转换为Json
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string List2Json<T>(List<T> list, Formatting formatting = Formatting.None)
        {
            return Object2Json(list, formatting);
        }

        /// <summary>
        /// 将Json转换为List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static List<T> Json2List<T>(string json)
        {
            return Json2Object<List<T>>(json);
        }
        #endregion

        #region Object和JObject相互转换
        /// <summary>
        /// 将Object转换为JObject
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static JObject Object2JObject(object obj)
        {
            return JObject.FromObject(obj);
        }

        /// <summary>
        /// 将JObject转换为Object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jo"></param>
        /// <returns></returns>
        public static T JObject2Object<T>(JObject jo)
        {
            return jo.ToObject<T>();
        }
        #endregion

        #region Dictionary和JObject相互转换
        /// <summary>
        /// 将Dictionary转换为JObject
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dic"></param>
        /// <returns></returns>
        public static JObject Dictionary2JObject<TKey, TValue>(Dictionary<TKey, TValue> dic)
        {
            return Object2JObject(dic);
        }

        /// <summary>
        /// 将JObject转换为Dictionary
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="jo"></param>
        /// <returns></returns>
        public static Dictionary<TKey, TValue> JObject2Dictionary<TKey, TValue>(JObject jo)
        {
            return JObject2Object<Dictionary<TKey, TValue>>(jo);
        }
        #endregion

        #region List和JArray相互转换
        /// <summary>
        /// 将List转换为JArray
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static JArray List2JArray<T>(List<T> list)
        {
            return JArray.FromObject(list);
        }

        /// <summary>
        /// 将JArray转换为List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ja"></param>
        /// <returns></returns>
        public static List<T> JArray2List<T>(JArray ja)
        {
            return ja.ToObject<List<T>>();
        }
        #endregion
    }
}
