﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FantasyGame.Utils
{
    public static class IdGenerater
    {
        public static ushort AppId { get; set; }

        private static ushort value;

        public static long GenerateId() {
            int time = (int)TimeHelper.UtcNowSeconds();

            return (AppId << 48) + (time << 16) + ++value;
        }
    }
}
