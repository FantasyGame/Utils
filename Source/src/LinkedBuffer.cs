﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FantasyGame.Utils
{
    public sealed class LinkedBuffer : IDisposable
    {
        public const int ChunkSize = 8192;

        private readonly LinkedList<byte[]> bufferList = new LinkedList<byte[]>();

        public int LastIndex { get; private set; }

        public int FirstIndex { get; private set; }

        public LinkedBuffer() {
            AddLast();
        }

        public int Count {
            get {
                int count = 0;
                if (this.bufferList.Count == 0)
                {
                    count = 0;
                }
                else {
                    count = (this.bufferList.Count - 1) * ChunkSize + LastIndex - FirstIndex;
                }

                if (count < 0) {
                    throw new ArgumentException($"LinkedBuffer's count < 0: {bufferList.Count}, {LastIndex}, {FirstIndex}");
                }
                return count;
            }
        }

        public void AddLast() {
            bufferList.AddLast(new byte[ChunkSize]);
        }

        public void RemoveFirst() {
            bufferList.RemoveFirst();
        }

        public byte[] First {
            get {
                if (bufferList.First == null) {
                    AddLast();
                }
                return bufferList.First.Value;
            }
        }

        public byte[] Last {
            get {
                if (bufferList.Last == null) {
                    AddLast();
                }
                return bufferList.Last.Value;
            }
        }

        public byte[] Read(int length) {
            if (length <= 0)
            {
                throw new ArgumentNullException();
            }

            byte[] data = new byte[length];
            int alreadyCount = 0;
            while (alreadyCount < length) {
                int n = length - alreadyCount;
                if (ChunkSize - FirstIndex > n)
                {
                    Array.Copy(First, FirstIndex, data, alreadyCount, n);
                    this.FirstIndex += n;
                    alreadyCount += n;
                }
                else {
                    Array.Copy(First, FirstIndex, data, alreadyCount, ChunkSize - FirstIndex);
                    alreadyCount += ChunkSize - FirstIndex;
                    RemoveFirst();
                    FirstIndex = 0;
                }
            }
            return data;
        }

        public void Write(byte[] data) {
            if (data == null || data.Length == 0) {
                throw new ArgumentNullException();
            }

            int alreadyCount = 0;
            while (alreadyCount < data.Length) {
                int n = data.Length - alreadyCount;
                if (ChunkSize - LastIndex > n)
                {
                    Array.Copy(data, alreadyCount, Last, LastIndex, n);
                    alreadyCount += n;
                    LastIndex += n;
                }
                else {
                    Array.Copy(data, alreadyCount, Last, LastIndex, ChunkSize - LastIndex);
                    alreadyCount += ChunkSize - LastIndex;
                    AddLast();
                    LastIndex = 0;
                }
            }
        }




        #region IDisposable
        bool isDisposed = false;

        public void Dispose()
        {
            if (isDisposed)
            {
                return;
            }
            isDisposed = true;

            bufferList.Clear();
        }
        #endregion
    }
}
