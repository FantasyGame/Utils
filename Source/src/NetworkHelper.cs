﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace FantasyGame.Utils
{
    /// <summary>
    /// 对于多字节整数值内的字节，不同的计算机使用不同的排序约定。 有些计算机首先放置最高有效字节（称为 Big-Endian 顺序），另外一些计算机首先放置最低有效字节（称为 Little-Endian 顺序）。 为了适用于使用不同字节排序方式的计算机，通过网络发送的所有整数值都按网络字节顺序发送，其中最高有效字节第一个发送。
    /// </summary>
    public static class NetworkHelper
    {
        public static uint NetworkToHostOrder(uint a)
        {
            return (uint)IPAddress.NetworkToHostOrder((int)a);
        }

        public static int NetworkToHostOrder(int a) {
            return IPAddress.NetworkToHostOrder(a);
        }

        public static ushort NetworkToHostOrder(ushort a)
        {
            return (ushort)IPAddress.NetworkToHostOrder((short)a);
        }

        public static short NetworkToHostOrder(short a)
        {
            return IPAddress.NetworkToHostOrder(a);
        }

        public static ulong NetworkToHostOrder(ulong a)
        {
            return (ulong)IPAddress.NetworkToHostOrder((long)a);
        }

        public static long NetworkToHostOrder(long a)
        {
            return IPAddress.NetworkToHostOrder(a);
        }

        public static uint HostToNetworkOrder(uint a)
        {
            return (uint)IPAddress.HostToNetworkOrder((int)a);
        }

        public static int HostToNetworkOrder(int a)
        {
            return IPAddress.HostToNetworkOrder(a);
        }

        public static ushort HostToNetworkOrder(ushort a)
        {
            return (ushort)IPAddress.HostToNetworkOrder((short)a);
        }

        public static short HostToNetworkOrder(short a)
        {
            return IPAddress.HostToNetworkOrder(a);
        }

        public static ulong HostToNetworkOrder(ulong a)
        {
            return (ushort)IPAddress.HostToNetworkOrder((long)a);
        }

        public static long HostToNetworkOrder(long a)
        {
            return IPAddress.HostToNetworkOrder(a);
        }
    }
}
