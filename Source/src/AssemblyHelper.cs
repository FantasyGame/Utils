﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace FantasyGame.Utils
{
    public class AssemblyHelper
    {
        private static AssemblyHelper _instance = new AssemblyHelper();

        private HashSet<string> _searchPath = new HashSet<string>();

        private AssemblyHelper() { }

        public static void AddSearchPath(string path)
        {
            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException(path);
            }
            _instance._searchPath.Add(path);
        }

        public static Assembly GetAssembly(string dll, string pdb = null)
        {
            Assembly assembly = null;

            string path = null;
            foreach (string _path in _instance._searchPath)
            {
                path = Path.Combine(_path, dll);
                if (File.Exists(path))
                {
                    byte[] dllBytes = File.ReadAllBytes(path);
                    path = Path.Combine(_path, pdb);
                    if (File.Exists(path))
                    {
                        byte[] pdbBytes = File.ReadAllBytes(path);
                        assembly = Assembly.Load(dllBytes, pdbBytes);
                    }
                    else
                    {
                        assembly = Assembly.Load(dllBytes);
                    }
                    return assembly;
                }
            }
            throw new FileNotFoundException(dll);
        }

        //public static Type[] GetTypes() {
        //    List<Type> types = new List<Type>();
        //    foreach()
        //}
    }
}
