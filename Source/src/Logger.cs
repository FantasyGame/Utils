﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FantasyGame.Utils
{
    public static class Logger
    {
        public static ILogger logger { get; set; }

        public static LogType logType { get; set; }

        public static void Trace(string format, params object[] objs)
        {
            if (IsLogTypeEnable(LogType.Trace))
            {
                LogEvent e = new LogEvent(objs.Length == 0 ? format : string.Format(format, objs), LogType.Trace);
                HandleLogEvent(e);
            }
        }

        public static void Log(string format, params object[] objs)
        {
            if (IsLogTypeEnable(LogType.Log))
            {
                LogEvent e = new LogEvent(objs.Length == 0 ? format : string.Format(format, objs), LogType.Log);
                HandleLogEvent(e);
            }
        }

        public static void Info(string format, params object[] objs)
        {
            if (IsLogTypeEnable(LogType.Info))
            {
                LogEvent e = new LogEvent(objs.Length == 0 ? format : string.Format(format, objs), LogType.Info);
                HandleLogEvent(e);
            }
        }

        public static void Warning(string format, params object[] objs)
        {
            if (IsLogTypeEnable(LogType.Warning))
            {
                LogEvent e = new LogEvent(objs.Length == 0 ? format : string.Format(format, objs), LogType.Warning);
                HandleLogEvent(e);
            }
        }

        public static void Error(string format, params object[] objs)
        {
            if (IsLogTypeEnable(LogType.Error))
            {
                LogEvent e = new LogEvent(objs.Length == 0 ? format : string.Format(format, objs), LogType.Error);
                HandleLogEvent(e);
            }
        }

        public static void Assert(bool isTrue, string format, params object[] objs)
        {
            if (!isTrue && IsLogTypeEnable(LogType.Assert))
            {
                LogEvent e = new LogEvent(objs.Length == 0 ? format : string.Format(format, objs), LogType.Assert);
                HandleLogEvent(e);
            }
        }

        public static void Mark(string format, params object[] objs)
        {
            if (IsLogTypeEnable(LogType.Mark))
            {
                LogEvent e = new LogEvent(objs.Length == 0 ? format : string.Format(format, objs), LogType.Mark);
                HandleLogEvent(e);
            }
        }

        private static void HandleLogEvent(LogEvent e)
        {
            logger.Handle(e);
        }

        private static bool IsLogTypeEnable(LogType logType)
        {
            return Logger.logType <= logType;
        }
    }

    public interface ILogger {
        void Handle(LogEvent e);
    }

    public class LogEvent
    {
        public string message { get; private set; }
        public LogType logType { get; private set; }
        public DateTime time { get; private set; }

        public LogEvent(string message, LogType logType)
        {
            this.message = message;
            this.logType = logType;
            time = DateTime.Now;
        }
    }

    public enum LogType : long {
        All = long.MinValue,
        Trace = 5000,
        Log = 10000,
        Info = 20000,
        Warning = 30000,
        Error = 40000,
        Assert = 50000,
        Mark = 2 << 53,
        Off = long.MaxValue,
    }
}
