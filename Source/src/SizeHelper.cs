﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FantasyGame.Utils
{
    public static class SizeHelper
    {
        public const int SByteSize = sizeof(sbyte);
        public const int Int16Size = sizeof(short);
        public const int Int32Size = sizeof(int);
        public const int Int64Size = sizeof(long);

        public const int ByteSize = sizeof(byte);
        public const int Uint16Size = sizeof(ushort);
        public const int Uint32Size = sizeof(uint);
        public const int Uint64Size = sizeof(ulong);
    }
}
