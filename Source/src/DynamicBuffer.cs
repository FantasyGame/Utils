﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FantasyGame.Utils
{
    public class DynamicBuffer : IDisposable
    {
        private readonly List<byte> buffer = new List<byte>();

        public int Count { get { return buffer.Count; } }

        public void Write(byte[] bytes) {
            buffer.AddRange(bytes);
        }

        public void Write(byte[] bytes, int offset, int size) {
            buffer.AddRange(bytes.Skip(offset).Take(size));
        }

        public byte[] Read(int offset, int size) {
            byte[] result = buffer.Skip(offset).Take(size).ToArray();
            buffer.RemoveRange(offset, size);
            return result;
        }

        public void Dispose()
        {
            buffer.Clear();
        }
    }
}
