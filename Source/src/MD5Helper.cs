﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace FantasyGame.Utils
{
    public static class MD5Helper
    {
        public static string FileMD5(string filePath) {
            byte[] retVal;
            using (FileStream file = new FileStream(filePath, FileMode.Open)) {
                MD5 md5 = new MD5CryptoServiceProvider();
                retVal = md5.ComputeHash(file);
            }

            return retVal.ToHex("x2");
        }

        public static string MD5(byte[] data) {
            byte[] retVal;
            MD5 md5 = new MD5CryptoServiceProvider();
            retVal = md5.ComputeHash(data);
            return retVal.ToHex("x2");
        }
    }
}
